package org.example.hashmap;

public class Node<K, V> {

    private K key;
    private V value;
    private Node<K, V> next;
    private Node<K, V> previous;

    public Node() {
    }

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
        this.next = null;
    }

    public void setNext(Node<K, V> next) {
        this.next = next;
    }

    public Node<K, V> getNext() {
        return next;
    }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public Node<K, V> getPrevious() {
        return previous;
    }

    public void setPrevious(Node<K, V> previous) {
        this.previous = previous;
    }

    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}
