package org.example.hashmap;

import java.util.Optional;

public class HashMapCustom<K, V> {

    private final Node<K, V>[] bucket;
    private final Integer capacity;

    public HashMapCustom(int capacity) {
        this.bucket = (Node<K, V>[]) (new Node[capacity]);
        this.capacity = capacity;
    }

    public void put(K key, V value) {
        int index = findIndex(key.hashCode());
        Node<K, V> node = new Node<>(key, value);
        if (bucket[index] == null) {
            bucket[index] = node;
        } else {
            checkAndUpdate(bucket[index], node);
        }
    }

    private void checkAndUpdate(Node<K, V> node, Node<K, V> newNode) {
        if (node.getKey().equals(newNode.getKey())) {
            node.setValue(newNode.getValue());
        } else if (node.getNext() == null) {
            node.setNext(newNode);
            newNode.setPrevious(node);
        } else {
            checkAndUpdate(node.getNext(), newNode);
        }
    }

    private int findIndex(int hashCode) {
        return hashCode % capacity;
    }

    public void printMap() {
        for (Node<K, V> node : bucket) {
            if (node != null)
                printNode(node);
        }
    }

    public V get(K key) {
        var node = bucket[findIndex(key.hashCode())];
        if (node != null) {
            var optFindedNode = find(node, key);
            return optFindedNode.map(Node::getValue).orElse(null);
        }
        return null;
    }

    private Optional<Node<K, V>> find(Node<K, V> node, K key) {
        if (node.getKey().equals(key)) {
            return Optional.of(node);
        } else if (node.getNext() == null) {
            return Optional.empty();
        } else {
            return find(node.getNext(), key);
        }
    }

    private void printNode(Node<K, V> node) {
        if (node.getNext() == null) {
            System.out.println(node);
        } else {
            System.out.println(node);
            printNode(node.getNext());
        }
    }

    public V remove(K key) {
        var node = bucket[findIndex(key.hashCode())];
        if (node != null) {
            var optFindedNode = find(node, key);
            if (optFindedNode.isPresent()) {
                var findedNode = optFindedNode.get();
                if (findedNode.getNext() != null) {
                    findedNode.getNext().setPrevious(findedNode.getPrevious());
                }
                if (findedNode.getPrevious() != null) {
                    findedNode.getPrevious().setNext(findedNode.getNext());
                }
                findedNode.setPrevious(null);
                findedNode.setNext(null);
                return findedNode.getValue();
            }
        }

        return null;
    }
}
