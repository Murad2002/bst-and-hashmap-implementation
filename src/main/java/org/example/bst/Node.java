package org.example.bst;

public class Node<T>{

    T data;
    Node<T> right;
    Node<T> left;
    Node<T> parent;

    public Node(T data) {
        this.data = data;
        right = left = parent = null;
    }

}
