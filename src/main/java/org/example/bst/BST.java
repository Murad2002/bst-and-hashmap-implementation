package org.example.bst;

public class BST<T> {

    public Node<T> root;

    public Long currentSize;

    public BST() {
        root = null;
        currentSize = 0L;
    }

    public void add(T obj) {
        Node<T> node = new Node<>(obj);
        if (root == null) {
            root = node;
            currentSize++;
            return;
        }
        add(root, node);
    }

    private void add(Node<T> parent, Node<T> newNode) {
        if (((Comparable<T>) newNode.data).compareTo(parent.data) < 0) {
            if (parent.left == null) {
                parent.left = newNode;
                newNode.parent = parent;
                currentSize++;
            } else {
                add(parent.left, newNode);
            }
        } else {
            if (parent.right == null) {
                parent.right = newNode;
                newNode.parent = parent;
                currentSize++;
            } else {
                add(parent.right, newNode);
            }
        }

    }

    public void delete(T node) {
        Node<T> nodeToDelete = find(node, root);
        if (nodeToDelete == null) {
            return;
        } else if (nodeToDelete.left != null) {
            nodeToDelete.left.parent = nodeToDelete.parent;
            if (nodeToDelete.parent != null && nodeToDelete.parent.right != null && nodeToDelete.parent.right.data == nodeToDelete.data)
                nodeToDelete.parent.right = nodeToDelete.left;
            else if (nodeToDelete.parent != null && nodeToDelete.parent.left != null && nodeToDelete.parent.left.data == nodeToDelete.data) {
                nodeToDelete.parent.left = nodeToDelete.left;
            }
            if (nodeToDelete.right != null) {
                Node<T> edgeRightNode = getEdgeRightNode(nodeToDelete.left);
                edgeRightNode.right = nodeToDelete.right;
                nodeToDelete.right.parent = edgeRightNode;
            }
        } else if (nodeToDelete.right != null) {
            nodeToDelete.right.parent = nodeToDelete.parent;
            if (nodeToDelete.parent != null)
                nodeToDelete.parent.right = nodeToDelete.right;
        } else if (nodeToDelete.parent != null && nodeToDelete.parent.right != null && nodeToDelete.parent.right.data == nodeToDelete.data) {
            nodeToDelete.parent.right = null;
        } else if (nodeToDelete.parent != null && nodeToDelete.parent.left != null && nodeToDelete.parent.left.data == nodeToDelete.data) {
            nodeToDelete.parent.left = null;
        }
        if (nodeToDelete.parent == null) {
            root = nodeToDelete.left;
        }
        nodeToDelete.parent = null;
        nodeToDelete.left = null;
        nodeToDelete.right = null;

    }

    public Node<T> getEdgeRightNode(Node<T> node) {
        if (node.right != null) {
            return getEdgeRightNode(node.right);
        } else
            return node;
    }

    public boolean contains(T obj) {
        return contains(obj, root);
    }

    private boolean contains(T obj, Node<T> root) {
        if (((Comparable<T>) obj).compareTo(root.data) == 0) {
            return true;
        } else if (((Comparable<T>) obj).compareTo(root.data) > 0) {
            if (root.right != null)
                return contains(obj, root.right);
            return false;
        } else {
            if (root.left != null)
                return contains(obj, root.left);
            return false;
        }
    }

    private Node<T> find(T obj, Node<T> root) {
        if (((Comparable<T>) obj).compareTo(root.data) == 0) {
            return root;
        } else if (((Comparable<T>) obj).compareTo(root.data) > 0) {
            if (root.right != null)
                return find(obj, root.right);
            return null;
        } else {
            if (root.left != null)
                return find(obj, root.left);
            return null;
        }
    }

    public void printInAscOrder(Node<T> node) {
        if (node == null)
            return;
        if (node.left != null) {
            printInAscOrder(node.left);
        }
        System.out.println(node.data);
        if (node.right != null) {
            printInAscOrder(node.right);
        }
    }

    public void printInDescOrder(Node<T> node) {
        if (node == null)
            return;
        if (node.right != null) {
            printInDescOrder(node.right);
        }
        System.out.println(node.data);
        if (node.left != null) {
            printInDescOrder(node.left);
        }
    }


}

