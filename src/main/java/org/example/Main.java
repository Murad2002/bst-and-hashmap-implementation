package org.example;

import org.example.bst.BST;
import org.example.hashmap.HashMapCustom;

public class Main {
    public static void main(String[] args) {
        BST<Integer> bst = new BST<>();
        bst.add(12);
        bst.add(8);
        bst.add(15);
        bst.add(6);
        bst.add(16);
        bst.add(5);
        bst.add(10);
        bst.add(4);
        bst.add(9);
        bst.add(11);
        bst.add(16);
        bst.add(14);

        bst.printInAscOrder(bst.root);
        System.out.println("--------------------");

        bst.printInDescOrder(bst.root);
        System.out.println("--------------------");

        System.out.println(bst.contains(6));
        System.out.println("--------------------");
        bst.printInAscOrder(bst.root);
        bst.delete(4);
        System.out.println("--------------");
        bst.printInAscOrder(bst.root);

        System.out.println("--------------------");
        System.out.println("--------------------");

        HashMapCustom<String, String> hashMapCustom = new HashMapCustom<>(20);
        for (int i = 0; i < 50; i++) {
            hashMapCustom.put(String.valueOf(i), "Test " + i);
        }
        hashMapCustom.put("1", "10000");
        hashMapCustom.printMap();
        System.out.println("Key 48 value : " + hashMapCustom.get("48"));
        System.out.println(hashMapCustom.remove("48"));
        System.out.println("Key 48 value : " + hashMapCustom.get("48"));
        System.out.println("--------------");
        hashMapCustom.printMap();

    }

}